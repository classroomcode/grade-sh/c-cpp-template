#include <stdio.h>
#include <errno.h>

static int
readline(char buf[], int size)
{   
    char c;
    int i = 0;
    do {
        if (i < size) {
            c = buf[i++] = fgetc(stdin); 
        }
        else {
            errno = ENOBUFS;
            return -1;
        }
    } while (c != '\n' && ! feof(stdin));
    buf[i-1] = 0;
    return i;
}

int 
main()
{   
    char buffer[1024];
    while (1) {  
        if (readline(buffer, sizeof(buffer)) < 0) {
            perror("readline");
            return 1;
        }
        else if (feof(stdin)) {
            return 0;
        }
        else {
            puts(buffer);
        }
    }
}
